<?php 
	if(!isset($_GET['ref']) || !wp_verify_nonce($_GET['ref'], '_resources_verify') && !empty($_GET['resid'])){
		wp_redirect( home_url('/resources', 302 ));
	}
	$postId = $_GET['resid'];
	$downTitle = get_the_title($postId);
	$downURL = get_post_meta( $postId, 'AB_resources_downloadLink', 'true' );
?>

<?php get_header(); ?>
	
	<section id="single-main" role="main" class="container gapless">

		<?php while(have_posts()): the_post(); ?>

		<section class="twelve left gapless data bgwhite">

			<section class="whole left gapless">	

				<article id="content" class="fourteen left gapless whitebg">
					<div class="whole gapless clearing"></div>
					<section id="article" class="clearing">
						<h2 class="whole centered gapless"><?php the_title(); ?></h2>
						<figure class="img whole gapless stretch" <?php if ( has_post_thumbnail($postId) ) : ?> data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($postId) ); ?>" <?php endif; ?>>
							<noscript><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($postId) ); ?>" /></noscript>
						</figure>
						<div class="whole gapless left"><?php $content = str_replace('[download_link]','<a id="download" href="'.$downURL.'" download="'.$downTitle.'.PDF">Download the Resource Here</a>',get_the_content()); echo apply_filters('the_content',$content); ?></div>
						
						<script>function startDownload () { jQuery('#download')[0].click(); } setTimeout (startDownload, 7 * 1000);</script>
					</section>

				</article>

			</section>

		</section>


		<aside id="sidebar" class="pushright gapless four">

			<?php get_template_part('sidebar'); ?>

		</aside>

		<?php endwhile; ?>

	</section>

<?php get_footer(); ?>