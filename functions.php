<?php

class Functions {

	public $class_prefix = '';
	private $rel_plugin_url;
	private $rel_assets_url;
	private $home_url;
	private $get_theme_name;
	private $theme_path;
	private $SIBurl;
	private $SIBkey;
	protected $fileName = '';

	public function __construct(){
		$this->class_prefix = 'ab_cal_';
		$this->fileName = basename(__FILE__);
		$this->SIBurl = "https://api.sendinblue.com/v2.0";
		$this->SIBkey = 't1pdfgRhcYGV0LQE';
		$this->init();
	}
	
	private function init(){
		if(!empty($_GET['export'])){
			$this->ab_cal_crms_export();
			die();
		}
		register_activation_hook( __FILE__, array($this, $this->class_prefix.'firstRunRequirements' ));
		$dbCheck = get_option('ab_crm_installed');
		if(!$dbCheck){
			$this->ab_cal_firstRunRequirements();
		}
		$this->ab_cal_theme_actions();
		$this->ab_cal_setup_rewrites();
		$this->ab_cal_tidy_wp();
		$this->ab_cal_hide_wp();
		$this->ab_cal_ajaxActions();
	}
	
	private function ab_cal_theme_actions(){
		add_action('init', array($this, $this->class_prefix.'dev_easter_eggs'));
		add_action('init', array($this, $this->class_prefix.'global_functions' ));
		add_action('admin_init', array($this, $this->class_prefix.'flush_rewrites'));
		add_action('init', array($this, $this->class_prefix.'setup_hotjobs'));
		add_action('init', array($this, $this->class_prefix.'setup_resources'));
		add_action('widgets_init', array($this, $this->class_prefix.'widgets_init'));
		add_action('add_meta_boxes', array($this, $this->class_prefix.'setup_cta_metabox'));
		add_filter('excerpt_length', array($this, $this->class_prefix.'excerpt_length'), 999 );
		add_action('admin_menu',array($this, $this->class_prefix.'admin_panels'));
		add_action( 'phpmailer_init', array($this, $this->class_prefix.'configure_smtp' ));
		add_action( 'init', array($this, $this->class_prefix.'hotjobs_taxonomy'));
		add_filter('post_type_link', array($this, $this->class_prefix.'filter_post_type_link'), 10, 2);
	}

	public function ab_cal_configure_smtp(PHPMailer $phpmailer ){
		$phpmailer->isSMTP(); //switch to smtp
	    $phpmailer->Host = 'smtp-relay.mailin.fr';
	    $phpmailer->SMTPAuth = true;
	    $phpmailer->Port = 587;
	    $phpmailer->Username = 'info@alta-blue.com';
	    $phpmailer->Password = 't1pdfgRhcYGV0LQE';
	    $phpmailer->SMTPSecure = false;
	    $phpmailer->From = 'news@alta-blue.com';
	    $phpmailer->FromName='Altablue News Blog';
	}

	public function ab_cal_firstRunRequirements(){
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}ab_crm_contacts` (
		  id mediumint(40) NOT NULL AUTO_INCREMENT,
		  email longtext NOT NULL,
		  name longtext  NOT NULL,
		  surname longtext  NOT NULL,
		  location longtext  NOT NULL,
		  role longtext  NOT NULL,
		  company longtext  NOT NULL,
		  subscribe varchar(100)  NOT NULL,
		  updated DATETIME DEFAULT NULL,
		  UNIQUE KEY id (id)
		);";
		print_r(dbDelta( $sql ));
		//die();
		update_option( "ab_crm_installed", '1');
	}

	public function ab_cal_admin_panels(){
		global $submenu;
		$page = add_menu_page('Altablue CRM', 'Altablue CRM', 'administrator' ,$this->fileName, array($this, 'ab_cal_crm_panel'));
	}

	public function ab_cal_excerpt_length( $length ) {
		return 1;
	}

	public function ab_cal_dev_easter_eggs(){
		header("X-SQL-Joke: A SQL query goes into a bar, walks up to two tables and asks, 'Can I join you?'");
	}

	public function ab_cal_global_functions(){
		if ( function_exists( 'add_theme_support' ) ) {
			add_theme_support( 'post-thumbnails' );
			add_image_size('featured_image', auto, 385, array( 'center', 'center' ));
		};		

		add_theme_support( 'menus' );
		register_nav_menus( array(
			'main_menu' => 'Main Menu'
		) );

		add_theme_support( 'featured-content', array(
			'featured_content_filter' => 'ab_cal_get_featured_posts',
			'max_posts' => 6,
		) );

	}

	private function ab_cal_setup_rewrites(){
		$theme_name = explode('/themes/', get_template_directory());
		$this->theme_name = next($theme_name);
		$this->home_url = array(home_url('/', 'http'), home_url('/', 'https'));
		$this->rel_plugin_url = $this->ab_cal_root_relative_url(str_replace($home_url, '', plugins_url()));
		$this->rel_assets_url = $this->ab_cal_root_relative_url(str_replace($home_url, '', content_url()));
		$this->theme_path = $this->rel_assets_url.'/themes/'.$this->theme_name.'/';
	}
	
	private function ab_cal_tidy_wp(){
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'index_rel_link');
		remove_action('wp_head', 'parent_post_rel_link', 10, 0);
		remove_action('wp_head', 'start_post_rel_link', 10, 0);
		remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
		add_action('wp_head', array($this, $this->class_prefix.'remove_recent_comments_style'), 1);
		add_filter('gallery_style', array($this,$this->class_prefix.'gallery_style'));
	}
	
	public function ab_cal_remove_recent_comments_style() {
	  	global $wp_widget_factory;
	  	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
			remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
	  	}
	}
	
	public function ab_cal_gallery_style($css) {
  		return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
	}
	
	public function ab_cal_add_rewrites($content) {
	  	global $wp_rewrite;
	  	$ab_cal_new_non_wp_rules = array(
			'css/(.*)'      => 'wp-content/themes/'. $this->theme_name . '/_assets/css/$1',
			'js/(.*)'       => 'wp-content/themes/'. $this->theme_name . '/_assets/js/$1',
			'imgs/(.*)'      => 'wp-content/themes/'. $this->theme_name . '/_assets/imgs/$1',
			'plugins/(.*)'  => 'wp-content/plugins/$1'
	  	);
	  	$wp_rewrite->non_wp_rules += $ab_cal_new_non_wp_rules;
	}

	public function ab_cal_clean_urls($content){
		$content = $this->ab_cal_root_relative_url($content).'/';
		if (strpos($content, $this->rel_plugin_url) !== false) {
			return str_replace($this->rel_plugin_url, '/plugins', $content);
		} else {
			return str_replace($this->theme_path, '/', $content);
		}
	}
	
	public function ab_cal_wp_nav_menu($text) {
		$replace = array(
		  'current-menu-item'     => 'active',
		  'current-menu-parent'   => 'active',
		  'current-menu-ancestor' => 'active',
		  'current_page_item'     => 'active',
		  'current_page_parent'   => 'active',
		  'current_page_ancestor' => 'active',
		);
	
	  	$text = str_replace(array_keys($replace), $replace, $text);
	  	return $text;
	}
	
	public function ab_cal_root_relative_url($input) {
		preg_match('|https?://([^/]+)(/.*)|i', $input, $matches);
		if (!isset($matches[1]) || !isset($matches[2])) {
			return $input;
		} elseif (($matches[1] === $_SERVER['SERVER_NAME']) || $matches[1] === $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) {
			return wp_make_link_relative($input);
		} else {
			return $input;
		}
	}
	
	public function ab_cal_fix_duplicate_subfolder_urls($input) {
	  	$output = $this->ab_cal_root_relative_url($input);
	  	preg_match_all('!([^/]+)/([^/]+)!', $output, $matches);
	
	  	if (isset($matches[1]) && isset($matches[2])) {
			if ($matches[1][0] === $matches[2][0]) {
		  		$output = substr($output, strlen($matches[1][0]) + 1);
			}
	  	}
	
	  	return $output;
	}
	
	private function ab_cal_hide_wp(){

		add_action('generate_rewrite_rules', array($this, $this->class_prefix.'add_rewrites'));
		add_filter('wp_nav_menu', array($this,$this->class_prefix.'wp_nav_menu'));

		if (!is_admin()) {
			$function_titles = array(
				'script_loader_src',
				'style_loader_src' 
			);
			$this->ab_cal_add_filter($function_titles, 'fix_duplicate_subfolder_urls');
			$function_titles = array(
				'plugins_url',
				'bloginfo', 
				'stylesheet_directory_uri',
				'template_directory_uri',
				'script_loader_src',
				'style_loader_src',
			);
			$this->ab_cal_add_filter($function_titles, 'clean_urls');
			if (!in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))) {
				$function_titles = array(
					'script_loader_src',
					'style_loader_src' 
				);
				$this->ab_cal_add_filter($function_titles, 'fix_duplicate_subfolder_urls');
				$function_titles = array(
					'bloginfo_url', 
					'theme_root_uri', 
					'stylesheet_directory_uri', 
					'template_directory_uri', 
					'plugins_url',
					'get_the_permalink',
					'wp_get_attachment_url',
					'the_permalink', 
					'wp_list_pages', 
					'wp_list_categories', 
					'wp_nav_menu', 
					'the_content_more_link', 
					'the_tags', 
					'get_pagenum_link', 
					'get_comment_link', 
					'month_link', 
					'day_link', 
					'year_link', 
					'tag_link', 
					'the_author_posts_link'
				);
				$this->ab_cal_add_action($function_titles, 'root_relative_url');
			}
		}	
	}

	private function ab_cal_add_action($titles,$function){
		foreach($titles as $title) {
			$full_func = $this->class_prefix.$function;
			add_action($title, array($this, $full_func));
		}
	}

	private function ab_cal_add_filter($titles,$function){
		foreach($titles as $title) {
			$full_func = $this->class_prefix.$function;
			add_filter($title, array($this, $full_func));
		}
	}

	public function ab_cal_flush_rewrites() {
	 	global $wp_rewrite;
	  	$wp_rewrite->flush_rules();
	}

	public function ab_cal_setup_hotjobs(){
		global $post;
		$labels = array(
		  'name' => _x( 'Hot Jobs', 'hotjobs' ),
		  'singular_name' => _x( 'Hot Job', 'hotjobs' ),
		  'add_new' => _x( 'Add New Hot Job', 'hotjobs' ),
		  'add_new_item' => _x( 'Add New Hot Job', 'hotjobs' ),
		  'edit_item' => _x( 'Edit Hot Job', 'hotjobs' ),
		  'new_item' => _x( 'New Hot Job', 'hotjobs' ),
		  'view_item' => _x( 'View Hot Job', 'hotjobs' ),
		  'search_items' => _x( 'Search Hot Jobs', 'hotjobs' ),
		  'not_found' => _x( 'No Hot Jobs found', 'hotjobs' ),
		  'not_found_in_trash' => _x( 'No Hot Jobs found in Trash', 'hotjobs' ),
		  'parent_item_colon' => _x( 'Parent Hot Job:', 'hotjobs' ),
		  'menu_name' => _x( 'Hot Jobs', 'hotjobs' ),
		  );
		$args = array(
		  'labels' => $labels,
		  'hierarchical' => false,
		  'description' => 'All the Hot Jobs from Altablue.',
		  'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail','custom-fields', 'sticky' ),
		  'public' => true,
		  'show_ui' => true,
		  'show_in_menu' => true,
		  'show_in_nav_menus' => true,
		  'publicly_queryable' => true,
		  'exclude_from_search' => false,
		  'has_archive' => true,
		  'query_var' => true,
		  'can_export' => true,
		  'rewrite' => array('slug' => 'hotjobs/viewall'),
		  'capability_type' => 'post',
		  'show_in_nav_menus' => true,
		  'taxonomies' => array('hotjobs_categories'),
		  'register_meta_box_cb' => array($this,$this->class_prefix.'add_hotjobs_metaboxs')
		);  
		register_post_type( 'hotjobs', $args );	
		flush_rewrite_rules();
	}

	public function ab_cal_hotjobs_taxonomy(){
		register_taxonomy(  
	        'hotjobs_categories', 
	        'hotjobs',
	        array(  
	            'hierarchical' => true,  
	            'label' => 'Hotjobs Categories',
	            'query_var' => true,
	            'rewrite' => array(
	                'slug' => 'hotjobs',
	                'with_front' => false 
	            )
	        )  
	    );
	}

	public function ab_cal_filter_post_type_link($link, $post){
		if ($post->post_type != 'hotjobs')
        	return $link;

	    if ($cats = get_the_terms($post->ID, 'hotjobs_categories'))
	        $link = str_replace('%hotjobs_categories%', array_pop($cats)->slug, $link);
	    	return $link;
	}

	public function ab_cal_setup_resources(){
		global $post;
		$labels = array(
		  'name' => _x( 'Resources', 'resources' ),
		  'singular_name' => _x( 'Resource', 'resources' ),
		  'add_new' => _x( 'Add New Resource', 'resources' ),
		  'add_new_item' => _x( 'Add New Resource', 'resources' ),
		  'edit_item' => _x( 'Edit Resource', 'resources' ),
		  'new_item' => _x( 'New Resource', 'resources' ),
		  'view_item' => _x( 'View Resource', 'resources' ),
		  'search_items' => _x( 'Search Resources', 'resources' ),
		  'not_found' => _x( 'No Resources found', 'resources' ),
		  'not_found_in_trash' => _x( 'No Resources found in Trash', 'resources' ),
		  'parent_item_colon' => _x( 'Parent Resource:', 'resources' ),
		  'menu_name' => _x( 'Resources', 'resources' ),
		  );
		$args = array(
		  'labels' => $labels,
		  'hierarchical' => false,
		  'description' => 'All the Resources from Altablue.',
		  'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail','custom-fields', 'sticky' ),
		  'public' => true,
		  'show_ui' => true,
		  'show_in_menu' => true,
		  'show_in_nav_menus' => true,
		  'publicly_queryable' => true,
		  'exclude_from_search' => false,
		  'has_archive' => false,
		  'query_var' => true,
		  'can_export' => true,
		  'rewrite' => array('slug' => 'resources'),
		  'capability_type' => 'post',
		  'show_in_nav_menus' => true,
		  'register_meta_box_cb' => array($this,$this->class_prefix.'add_resources_metaboxs')
		);  
		register_post_type( 'resources', $args );	
		flush_rewrite_rules();
	}

	public function ab_cal_widgets_init() {

		register_sidebar( array(
			'name'          => 'Resources',
			'id'            => 'sidebar',
			'before_widget' => '<section id="res" class="clearing">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		) );

		register_widget( 'ab_res_widget' );

	}

	private function ab_cal_ajaxActions(){
		if(method_exists($this,$this->class_prefix.'SIB_emailCheck')){
			add_action( 'wp_ajax_ab_cal_SIB_emailCheck', array($this ,$this->class_prefix.'SIB_emailCheck') ); 
			add_action( 'wp_ajax_nopriv_ab_cal_SIB_emailCheck', array($this ,$this->class_prefix.'SIB_emailCheck') ); 
		}
		if(method_exists($this,$this->class_prefix.'SIB_signup')){
			add_action( 'wp_ajax_ab_cal_SIB_signup', array($this ,$this->class_prefix.'SIB_signup') ); 
			add_action( 'wp_ajax_nopriv_ab_cal_SIB_signup', array($this ,$this->class_prefix.'SIB_signup') ); 
		}
		if(method_exists($this,$this->class_prefix.'SIB_subscribe')){
			add_action( 'wp_ajax_ab_cal_SIB_subscribe', array($this ,$this->class_prefix.'SIB_subscribe') ); 
			add_action( 'wp_ajax_nopriv_ab_cal_SIB_subscribe', array($this ,$this->class_prefix.'SIB_subscribe') ); 
		}
	}

	public function ab_cal_SIB_api($method,$apiCall,$data=null){
		if(empty($method)){
			die(json_encode(array('status' => 'error', 'message' => 'no method provided')));
		}
		$url = $this->SIBurl."/".$apiCall;
		$args = array(
			'headers' => array(
				'Content-type' => 'Content-Type:application/json',
				'api-key' => $this->SIBkey
			),
			'method' => $method
		);
		if(!empty($data)){
			$args['body'] = json_encode($data);
		}
		$response = wp_remote_request($url,$args);
		if ( is_wp_error( $response ) ) {
		   $error_message = $response->get_error_message();
		   return json_encode(array('status' => 'error', 'message' => $error_message));
		} else {
		   return json_encode(array('status' => 'success', 'message' => $response));
		}
	}

}

if(!is_admin()){
	require_once('_assets/includes/functions.front.php');
	require_once('_assets/includes/functions.walker.php');
}else{
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		require_once('_assets/includes/functions.front.php');
	}
	require_once('_assets/includes/functions.admin.php');
	require_once('_assets/includes/functions.customTable.php');
}

require_once('_assets/includes/functions.featured_content.php');
require_once('_assets/includes/functions.widget.php');

?>