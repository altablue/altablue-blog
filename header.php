<!DOCTYPE html id="root" lang="en">
<!--[if lt IE 7 ]> <html class="ie6" id="root" lang="en"> <![endif]-->
<!--[if IE 7 ]> <html class="ie7" id="root" lang="en"> <![endif]-->
<!--[if IE 8 ]> <html class="ie8" id="root" lang="en"> <![endif]-->
<!--[if gt IE 8 ]> <html class="ie9" id="root" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html id="root" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php wp_title( '|', true, 'right' );if ( $site_description && ( is_home() || is_front_page() ) ) {echo " | $site_description";} else { echo  " | ".get_the_title(); } ?></title>
    <html itemscope itemtype="http://schema.org/Article">
    
    <?php if(is_home()): ?>
        
        <meta itemprop="name" content="Altablue">
        <meta itemprop="description" content="<?php wp_title( '|', true, 'right' );if ( is_home() || is_front_page() ){ echo " | ".get_the_title(); } else{ echo " | ".get_the_excerpt(); } ?>">
        <meta itemprop="image" content="http://blog.alta-blue.com/wp-content/themes/altablue/_assets/imgs/site-prev.jpg">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Altablue">
        <meta name="twitter:description" content="<?php wp_title( '|', true, 'right' );if ( is_home() || is_front_page()){ echo " | ".get_the_title(); } else{ echo " | ".get_the_excerpt(); } ?>">
        <meta name="twitter:image:src" content="http://blog.alta-blue.com/wp-content/themes/altablue/_assets/imgs/site-prev.jpg">

        <meta property="og:title" content="Altablue" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="http://blog.alta-blue.com" />
        <meta property="og:image" content="http://blog.alta-blue.com/wp-content/themes/altablue/_assets/imgs/site-prev.jpg" />
        <meta property="og:description" content="<?php wp_title( '|', true, 'right' );if ( is_home() || is_front_page()){ echo " | ".get_the_title(); } else{ echo " | ".get_the_excerpt(); } ?>" />

    <?php endif; ?>

    <meta property="og:site_name" content="Altablue" />

    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

    <meta name="author" content="Altablue">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/wp-content/themes/altablue/_assets/css/main.css">
    <link rel="shortcut icon" href="/wp-content/themes/altablue/_assets/imgs/favicon.ico"/>

    <meta name="google-site-verification" content="lA9LJwp9rIQDwWMIVwGGLg6JDbnaUm-cyUgWg4XZ7bQ" />

    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->

    <script src="/wp-content/themes/altablue/_assets/js/libs/browser-detect.js"></script>
    <script src="/wp-content/themes/altablue/_assets/js/libs/modernizr.min.js" ></script>
    <link rel="stylesheet" href="/wp-content/themes/altablue/_assets/css/jquery.stickyheaders.css">
    <?php wp_head(); ?>
</head>
<body>

    <nav id="candid-photography-sketch">
        <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu' => 'main_menu', 'container' => 'ul','container_class' => 'blog_main_menu clearing') ); ?>
    </nav>

    <section class="wrapper">

        <header class="container">
            <section id="logo" class="six left gapless">
                <a href="<?php echo home_url(); ?>" class="whole gapless" target="_blank"><div class="icon-ablue logo five left gapless"></div><p class="eleven gapless left"><small>Great People Doing Great Things</small></p></a>
            </section>
            <section id="ab_menu" class="pushright two gapless">
                <nav>
                    <ul class="whole gapless first">
                        <li><a href="#ab_menu" id="placeholder" class="icon-door">Altablue Website</a></li>
                        <li></li>
                        <ul class="whole gapless children">
                            <li><a href="http://alta-blue.com/home.html" target="_blank" class="icon-question">About Altablue</a></li>
                            <li><a href="https://jobs.alta-blue.com" target="_blank" class="icon-jobs">Jobs Board</a></li>
                            <li><a href="http://alta-blue.com/contact.html" target="_blank" class="icon-phone">Contact Altablue</a></li>
                        </ul>
                    </ul>
                </nav>
            </section> 
        </header>

        <section id="secondHeader" class="container">
            <div class="spacer one gapless left"></div>
            <nav id="blog_menu" class="pushright twelve gapless">
                <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu' => 'main_menu', 'container' => 'ul','container_class' => 'blog_main_menu clearing') ); ?>
            </nav>
            <div id="elvis">
                <span class="bar1"></span>
                <span class="bar2"></span>
                <span class="bar3"></span>
            </div>
        </section>