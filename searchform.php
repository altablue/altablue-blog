<form role="search" method="get" id="searchform" class="searchform clearing whole gapless" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="clearing">
		<label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
		<input placeholder="Search the blog..." type="text" class="thirteen left" value="<?php echo get_search_query(); ?>" name="s" id="s"/>
		<input type="submit" class="right three icon-search" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
	</div>
</form>