    <label for="filter-by-date"><?php _e( 'Show Contacts Captured' ); ?></label>
    <select name="date_op">
		<option value="1" <?php echo (!empty($_GET['date_op']) && $_GET['date_op'] == 1?'selected="selected"':''); ?>><?php _e( 'During' ); ?></option>
		<option value="2" <?php echo (!empty($_GET['date_op']) && $_GET['date_op'] == 2?'selected="selected"':''); ?>><?php _e( 'Up To' ); ?></option>
		<option value="3" <?php echo (!empty($_GET['date_op']) && $_GET['date_op'] == 3?'selected="selected"':''); ?>><?php _e( 'After' ); ?></option>
    </select>
    <select name="date" id="filter-by-date">
        <option value="0"><?php _e( 'All dates' ); ?></option>
        <?php echo $output; ?>
    </select>
    <input type="submit" value="Filter" class="button" id="post-query-submit" name="filter_action">

    <button type="submit" formtarget="_blank" value="exportcsv" class="button button-primary button-large" target="_blank" name="export">Export as CSV</button>