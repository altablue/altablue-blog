<?php if(!empty($cta_data)): ?>

	<h4>CTA preview</h4>
	<hr/>
	<div class="clear">
		<figure style="max-height:150px;max-width:300px;overflow:hidden;float:left;width:20%;">
			<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($cta_data->ID) ); ?>" style="width:100%;" />
		</figure>
		<section style="float:left;width:70%">
			<h2><?php echo $cta_data->post_title; ?></h2>
			<p><?php echo $cta_data->post_excerpt; ?></p>
		</section>
		<div class="clear"></div>
	</div>
	<hr/>

<?php endif; ?>

<label for="post_cta"><?php _e('Select Resource:');?></label>
<select class="large-text" id="post_cta" name="post_cta">
	<option value="0" <?php echo (empty($post_cta)? 'selected="selected"': ''); ?>>Please select a resource...</option>
	<?php foreach($resources as $res): ?>
		<option value="<?php echo esc_attr($res->ID); ?>" <?php echo (!empty($post_cta) && $post_cta === $res->ID? 'selected="selected"': ''); ?>><?php echo esc_attr($res->post_title);?></option>
	<?php endforeach; ?>
</select>