<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
	<input class="large-text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />

	<label for="<?php echo $this->get_field_id( 'resource' ); ?>"><?php _e('Select Resource:');?></label>
	<select class="large-text" id="<?php echo $this->get_field_id( 'resource' ); ?>" name="<?php echo $this->get_field_name( 'resource' ); ?>">
		<option value="0" <?php echo (empty($selectedRes)? 'selected="selected"': ''); ?>>Please select a resource...</option>
		<?php foreach($resources as $res): ?>
			<option value="<?php echo esc_attr($res->ID); ?>" <?php echo (!empty($selectedRes) && $selectedRes === $res->ID? 'selected="selected"': ''); ?>><?php echo esc_attr($res->post_title);?></option>
		<?php endforeach; ?>
	</select>

</p>