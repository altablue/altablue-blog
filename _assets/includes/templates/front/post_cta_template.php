<section id="cta" class="clearing">
	<figure class="half left gapless">
		<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($cta_data->ID) ); ?>" />
	</figure>
	<div class="half pushright whitebg gapless content info">
		<h4>RELATED MATERIAL</h4>
		<h5><?php echo $cta_data->post_title; ?></h5>
		<p><?php echo $cta_data->post_excerpt; ?></p>
		<a href="<?php echo $cta_data->guid; ?>" class="cta-orange cta orange">download</a>
	</div>
</section>
