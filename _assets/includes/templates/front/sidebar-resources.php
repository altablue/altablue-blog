	<?php echo $args['before_widget']; ?>

	<?php if ( ! empty( $title ) ) echo $args['before_title'] . $title . $args['after_title']; ?>

	<hr />
	<figure class="third left gapless">
		<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($res->ID) ); ?>" />
	</figure>
	<div class="twothirds pushright gapless content info">
		<h5><?php echo $res->post_title; ?></h5>
		<a href="<?php echo get_the_permalink($res->ID); ?>" class="cta-orange cta orange">download</a>
	</div>

	<?php echo $args['after_widget']; ?>