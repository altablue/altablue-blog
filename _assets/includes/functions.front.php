<?php

class Functions_front extends Functions{

	private $bitlytoken;
	
	public function __construct(){
		parent::__construct();
		$this->bitlytoken = "6d55d1d5d4273a0b62b34f219752261d3867e839";
		$this->ab_cal_front_init();
	}
	
	private function ab_cal_front_init(){
		add_filter( 'excerpt_length', array($this, $this->class_prefix.'excerpt_length'), 999 );
		add_action('wp_head', array($this, $this->class_prefix.'add_ajaxUrl'));
	}

	public function ab_cal_add_ajaxUrl(){
		echo "<script type='text/javascript'>var ajaxurl = '".admin_url('admin-ajax.php')."';</script>";
	}

	public function ab_cal_excerpt_length( $length ) {
		return 12;
	}

	public function ab_cal_is_latest_post( $post_id, $query_args = array() ){
		static $latest_post_id = false;
		$post_id = empty( $post_id  ) ? get_the_ID() : $post_id ;
		$stickyIds = array_slice( get_option('sticky_posts', array() ), 0, 1000);
		$stickyIds = implode(',',$stickyIds);
		if( !$latest_post_id ){
		  $query_args['numberposts'] = 1;
		  $query_args['post_status'] = 'publish';
		  $query_args['exclude'] = $stickyIds;
		  $last = wp_get_recent_posts( $query_args );
		  $latest_post_id = $last['0']['ID'];
		}

		if($latest_post_id == $post_id){
			return true;
		}else{
			return false;
		}
	}

	public function ab_cal_time_ago( $type = 'post' ) {
		$d = 'comment' == $type ? 'get_comment_time' : 'get_post_time';
		return human_time_diff($d('U'), current_time('timestamp')) . " " . __('ago');
	}
	
	public function ab_cal__pagination($pages = '', $range = 1){  

	}
	
	public function ab_cal_number_postpercat($idcat) {
		global $wpdb;
		$query = $wpdb->get_results("SELECT count FROM ".$wpdb->term_taxonomy." WHERE term_id = ".$idcat);
		echo $query[0]->count;
	}
	
	public function ab_cal_makeBitlyUrl($url) {  
    	$bitly_reply = wp_remote_get('https://api-ssl.bitly.com/v3/shorten?access_token='.$this->bitlytoken.'&longUrl='.$url.'&format=json');
    	$bitly_decoded = json_decode($bitly_reply['body']);
    	return urldecode($bitly_decoded->data->url);
	}

	public function ab_cal_get_latest_hotjobs(){
		$past = strtotime("-1 month", time());
		$stickies = get_option('CPT_hotjobs_sticky');
		$ab_cal_hotjobs = new WP_Query(array(
	        'post_type' => 'hotjobs',
			'date_query' => array(
				array(
					'after'     =>  array(
						'year' => date('Y', $past),
						'month' => date('n', $past),
						'day' => 1,
					),
					'before'    => array(
						'year' => date('Y'),
						'month' => date('n'),
						'day' => date('j'),
					),
					'inclusive' => true,
				),
			),
			'posts_per_page' => 4,
			'post__not_in' => $stickies
	    ));
	    wp_reset_postdata();
	    return $ab_cal_hotjobs;
	}

	public function ab_cal_get_sticky_hotjobs(){
		$past = strtotime("-1 month", time());
		$stickies = get_option('CPT_hotjobs_sticky');
		$ab_cal_sticky_hotjobs = new WP_Query( array(
			'post_type' => 'hotjobs',
			'posts_per_page'      => -1,
			'post__in'            => $stickies,
			'ignore_sticky_posts' => 0,
		));
		wp_reset_postdata();
	    return $ab_cal_sticky_hotjobs;
	}

	public function ab_cal_get_resources_list(){
		$type = 'resources';
		$args=array(
			'post_type' => $type,
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'caller_get_posts'=> 1
		);
		$ab_cal_res = null;
		$ab_cal_res = new WP_Query($args);
		wp_reset_query(); 
	    return $ab_cal_res;
	}

	public function ab_cal_the_post_thumbnail_caption() {
	  global $post;

	  $thumbnail_id    = get_post_thumbnail_id($post->ID);
	  $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
	  if ($thumbnail_image && isset($thumbnail_image[0])) {
	    echo '<span>'.$thumbnail_image[0]->post_excerpt.'</span>';
	  }
	}

	public function ab_cal_the_post_cta(){
		global $post;

		$post_cta = get_post_meta( $post->ID, 'ab_post_cta', 'true' );

		if(!empty($post_cta)){
			$cta_data = get_post( $post_cta, OBJECT );
			$url = get_post_meta( $cta_data->ID, 'AB_resources_downloadLink', 'true' );
	
			include_once('templates/front/post_cta_template.php');
		}
	}

	public function ab_cal_SIB_emailCheck(){
		$email = false;
		$data = array();
		if(is_email($_POST['data']['email'])){
			$email = sanitize_text_field($_POST['data']['email']);
			$data['email'] = $email; 
			echo $SIBdata = $this->ab_cal_SIB_api('GET', "user/".$email);
		}else{
			echo json_encode(array('status'=>'error', 'message'=>'no vaild email provided'));
		}
		die();
	}

	public function ab_cal_SIB_subscribe(){
		global $post;
		$email = false;
		$data = array();
		parse_str($_POST['data'], $data);
		if(is_email($data['email'])){
			$array = $SIB_array = array();
			foreach($data as $key => $field){
				$array[$key] = sanitize_text_field($field);
				if($key == 'NAME' || $key == 'SURNAME'){
					$SIB_array['attributes'][$key] = sanitize_text_field($field);
				}
				if($key == 'resid'){
					$resid = sanitize_text_field($field);
				}
			}

			$SIB_array['email'] = sanitize_text_field($data['email']);
			$SIB_array['listid'] = array(16);//array(sanitize_text_field($list_id['listid']));

			$return = json_decode($SIBdata = $this->ab_cal_SIB_api('PUT', "user/".$data['email'], $SIB_array));
			$bodyMessage = json_decode($return->message->body);
			if($bodyMessage->code == 'success'){
				$name = '';
				if(!empty($SIB_array['attributes']['NAME'])){
					$name .= $SIB_array['attributes']['NAME'].' ';
				}
				if(!empty($SIB_array['attributes']['SURNAME'])){
					$name .= $SIB_array['attributes']['SURNAME'];
				}
				$message = '
Hey '.$name.',

Thanks so much for subscribing to our blog newsletter. By subscribing, you have ensured that you will get a round up of all the new content that lands on our blog, straight into your inbox, hi-five!

If you would like to ask us some questions specific to your business goals or want to chat about anything at all, just get in touch at info@alta-blue.com. 

We would love to hear the reasons behind your brilliant choice in subscribing to our blog and any feedback you want to offer to us.

Regards,

Altablue
				';
				$mail = wp_mail($data['email'], 'Thanks for Subscribing to Altablue News',$message,$headers,$attach);
				if($mail){
					echo $SIBdata;
					//echo json_encode(array('status'=>'success', 'message'=> array('body' => array('status' => array('code' => 'success')))));
				}
			}
		}else{
			echo json_encode(array('status'=>'error', 'message'=>'no vaild email provided'));
		}
		die();
	}

	public function ab_cal_SIB_signup(){
		global $post;
		$email = false;
		$data = array();
		parse_str($_POST['data'], $data);
		if(is_email($data['email'])){
			$array = $SIB_array = array();
			foreach($data as $key => $field){
				$array[$key] = sanitize_text_field($field);
				if($key == 'NAME' || $key == 'SURNAME' || $key == 'LOCATION' || $key == 'ROLE' || $key == 'COMPANY' || $key == 'subscribe'){
					$SIB_array['attributes'][$key] = sanitize_text_field($field);
				}
				if($key == 'resid'){
					$resid = sanitize_text_field($field);
				}
				$list_id = get_option('sib_home_option');
			}

			$SIB_array['email'] = sanitize_text_field($data['email']);
			if($SIB_array['attributes']['subscribe'] == true){
				$SIB_array['listid'] = array(16);//sanitize_text_field($list_id['listid']);
			}
			$SIB_array['downloads'] = $resid;

			$return = json_decode($SIBdata = $this->ab_cal_SIB_api('PUT', "user/".$data['email'], $SIB_array));
			$bodyMessage = json_decode($return->message->body);
			if($bodyMessage->code == 'success'){
				if(!$db = $this->ab_cal_CRM_update($SIB_array)){
					$name = '';
					$downURL = get_post_meta( $resid, 'AB_resources_downloadLink', 'true' );
					$attach = array( $downURL );
					if(!empty($SIB_array['attributes']['NAME'])){
						$name .= $SIB_array['attributes']['NAME'].' ';
					}
					if(!empty($SIB_array['attributes']['SURNAME'])){
						$name .= $SIB_array['attributes']['SURNAME'];
					}
					$message = '
Hey '.$name.',

Thanks so much for deciding to download our resource "'.get_the_title($resid).'", we hope you enjoy reading it and find that it helps bring added success to you and your business.

You can also access the download from the follow url:

'.$downURL.'

You can access this download at this URL 24/7 so it is always there for when you need it.

If you would like to ask us some questions specific to your business goals or want to chat about anything at all, just get in touch at info@alta-blue.com. 

We would love to hear how you found the resource and any feedback you want to give us on it.

Regards,

Altablue
					';
					$mail = wp_mail($data['email'], 'Your Free Download from Altablue',$message,$headers,$attach);
					if($mail){
						echo $SIBdata;
						//echo json_encode(array('status'=>'success', 'message'=> array('body' => array('status' => array('code' => 'success')))));
					}
				}else{
					echo json_encode(array('status'=>'error', 'message'=>$db));
				}
			}else{
				echo json_encode(array('status'=>'error', 'message'=>'error adding use to SIB'));
			}
		}else{
			echo json_encode(array('status'=>'error', 'message'=>'no vaild email provided'));
		}
		die();
	}

	private function ab_cal_CRM_update($contact){
		global $wpdb;
		//$wpdb->show_errors();
		$array = array();
		$array = array_change_key_case($contact['attributes'],CASE_LOWER);
		$array['email'] = $contact['email'];
		$array['updated'] = date("Y-m-d H:i:s");
		if(!$wpdb->update($wpdb->prefix.'ab_crm_contacts', $array, array('email' => $contact['email']), array('%s','%s','%s','%s','%s','%s','%s','%s'), array('%s'))){
			$wpdb->insert($wpdb->prefix.'ab_crm_contacts', $array, array('%s','%s','%s','%s','%s','%s','%s','%s'));
			$wpId = $wpdb->insert_id;
		}
		$array['downloads'] = $contact['downloads'];
		$dbRow = $wpdb->get_results('SELECT downloads from '.$wpdb->prefix.'ab_crm_contacts where email="'.$contact['email'].'"');
		if(!empty($dbRow)){
			$tmp['downloads'] = $dbRow[0]->downloads.','.$contact['downloads'];
		}else{
			$tmp['downloads'] = $contact['downloads'];
		}
		$wpdb->update($wpdb->prefix.'ab_crm_contacts', $tmp, array('email' => $contact['email']), array('%s',), array('%s'));
		echo $wpdb->print_error();
	}

}

$ab_cal_functions = new Functions_front;

?>