<?php

class Functions_admin extends Functions{
	
	public function __construct(){
		parent::__construct();
		$this->ab_cal_admin_init();
	}
	
	private function ab_cal_admin_init(){
		$this->ab_cal_admin_actions();
		$this->ab_cal_admin_filters();
	}

	private function ab_cal_admin_actions(){
		add_action( 'save_post', array($this, $this->class_prefix.'save_page_meta'));
		add_action( 'manage_hotjobs_posts_custom_column', array($this, $this->class_prefix.'hotjobs_featured'), 10, 2 );
	}

	private function ab_cal_admin_filters(){
		add_filter('manage_hotjobs_posts_columns', array($this, $this->class_prefix.'hotjobs_cols'));
	}

	public function ab_cal_hotjobs_cols( $defaults ) {
	    $defaults['featured']  = 'Featured Job';
	    return $defaults;
	}

	public function ab_cal_hotjobs_featured( $column_name, $post_id ) {
	    if ($column_name == 'featured') {
	    	$stickies = get_option('CPT_hotjobs_sticky');
	    	if(in_array($post_id, $stickies)){
	    		echo '<strong>Featured Job</strong>';
	    	}else{
	    		return false;
	    	}
	    }
	}

	public function ab_cal_add_hotjobs_metaboxs(){
		add_meta_box('ab_cal_hotjobs_sticky_box', 'Featured Hot Job', array($this, $this->class_prefix.'hotjobs_sticky_box'), 'hotjobs', 'side', 'default');
		add_meta_box('ab_cal_hotjobs_irec_box', 'Hot Job IREC Code', array($this, $this->class_prefix.'hotjobs_irec_box'), 'hotjobs', 'side', 'default');
	}

	public function ab_cal_hotjobs_sticky_box(){
		global $post;
		$CPT_hotjobs_stickies = get_option('CPT_hotjobs_sticky');
		include_once('templates/admin/hotdeals_sticky_metabox.php');
	}

	public function ab_cal_hotjobs_irec_box(){
		global $post;
		$CPT_hotjobs_irec = get_post_meta( $post->ID, 'CPT_hotjobs_irec', 'true' );
		include_once('templates/admin/hotdeals_irec_metabox.php');
	}

	public function ab_cal_save_page_meta(){
		global $post;
		if(!empty($_POST['CPT_hotjobs_sticky'])){
			$CPT_hotjobs_stickies = array();
			$CPT_hotjobs_stickies = get_option('CPT_hotjobs_sticky');
			if(!in_array($_POST['CPT_hotjobs_sticky'], $CPT_hotjobs_stickies)){
				$CPT_hotjobs_stickies[$_POST['CPT_hotjobs_sticky']] = $_POST['CPT_hotjobs_sticky'];
			}
			update_option( 'CPT_hotjobs_sticky', $CPT_hotjobs_stickies );
		}else{
			$CPT_hotjobs_stickies = array();
			$CPT_hotjobs_stickies = get_option('CPT_hotjobs_sticky');
			unset($CPT_hotjobs_stickies[$post->ID]);
			update_option( 'CPT_hotjobs_sticky', $CPT_hotjobs_stickies );
		}
		if(!empty($_POST['CPT_hotjobs_irec'])){
			$CPT_hotjobs_irecs = array();
			$CPT_hotjobs_irecs = sanitize_text_field($_POST['CPT_hotjobs_irec']);
			update_post_meta($post->ID, 'CPT_hotjobs_irec', $CPT_hotjobs_irecs );
		}else{
			$CPT_hotjobs_irecs = array();
			delete_post_meta($post->ID, 'CPT_hotjobs_irec');
		}
		if(!empty($_POST['AB_resources_downloadLink'])){
			$AB_resources_downloadLink = array();
			$AB_resources_downloadLink = esc_url($_POST['AB_resources_downloadLink']);
			update_post_meta($post->ID, 'AB_resources_downloadLink', $AB_resources_downloadLink );
		}else{
			delete_post_meta($post->ID, 'AB_resources_downloadLink');
		}
		if(!empty($_POST['post_cta'])){
			$post_cta = sanitize_text_field($_POST['post_cta']);
			update_post_meta($post->ID, 'ab_post_cta', $post_cta );
		}else{
			delete_post_meta($post->ID, 'ab_post_cta');
		}
	}

	public function ab_cal_add_resources_metaboxs(){
		add_meta_box('ab_cal_resources_download_link', 'Resource Download URL', array($this, $this->class_prefix.'resources_download_box'), 'resources', 'advanced', 'default');
	}

	public function ab_cal_resources_download_box(){
		global $post;
		$res_downloadLink = get_post_meta( $post->ID, 'AB_resources_downloadLink', 'true' );
		include_once('templates/admin/resources_download_metabox.php');
	}

	public function ab_cal_setup_cta_metabox(){
		add_meta_box('ab_cal_post_cta', 'Resource CTA for this Post', array($this, $this->class_prefix.'post_cta_box'), 'post', 'advanced', 'default');
	}

	public function ab_cal_post_cta_box(){
		global $post, $wpdb;

		$post_cta = get_post_meta( $post->ID, 'ab_post_cta', 'true' );
		$resources = $wpdb->get_results("SELECT * FROM ".$wpdb->posts." WHERE post_type = 'resources' AND post_status='publish'");

		if(!empty($post_cta)){
			$cta_data = get_post( $post_cta, OBJECT );
		}

		include_once('templates/admin/posts_cta_metabox.php');
	}

	public function ab_cal_crm_panel(){
		global $wpdb;

		$where = false;
		$method = 1;

		if(!empty($_GET['export'])){
			$this->ab_cal_crms_export();
			die();
		}else{
			$where = $this->ab_cal_crm_date();
		}

		$tableData = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ab_crm_contacts ".$where." ORDER BY id DESC", ARRAY_A);

		foreach($tableData as $cols => $td){
			if(!empty($td['downloads']) && $td['downloads'] !== NULL && $td['downloads'] != null){
				$array =  array_unique(explode(',', $td['downloads']));
				$tmp = '';
				foreach($array as $ar){
					$title = get_the_title($ar);
					$tmp .= ',<strong>'.$title.'</strong>';
				}
				$tableData[$cols]['downloads'] = trim($tmp,',');
			}
		}

		$eDate = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ab_crm_contacts ORDER BY updated ASC limit 1", ARRAY_A);

		$cols = $wpdb->get_col_info();
		$columns = array();

		foreach($cols as $col){
				$columns[strtolower($col)] = ucfirst(strtolower($col));
		}

	    $crmTable = new Functions_customTable($eDate[0]['updated']);
	    $crmTable->prepare_items($tableData,$columns, NULL);
    
		include_once('templates/admin/crm_panel.php');
	}

	private function ab_cal_crm_date(){
		if(!empty($_GET['date'])){
			$cleanDate = sanitize_text_field($_GET['date']);
			parse_str($cleanDate,$urldate);
			$date = explode('-',key($urldate));
			if(!empty($_GET['date_op']) && is_numeric($_GET['date_op'])){
				$method = $_GET['date_op'];
			}

			switch ($method) {
				case 2:
					$op = '<=';
					break;
				case 3:
					$op = '>=';
					break;
				default:
					$op = '=';
					break;
			}
			return $where = "WHERE MONTH(updated)".$op.$date[0]." && YEAR(updated)".$op.$date[1];
		}
	}

	public function ab_cal_crms_export(){
		global $wpdb;

		$where = $this->ab_cal_crm_date();

		$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ab_crm_contacts ".$where." ORDER BY id DESC", ARRAY_A);

		foreach($data as $cols => $td){
			if(!empty($td['downloads']) && $td['downloads'] !== NULL && $td['downloads'] != null){
				$array =  array_unique(explode(',', $td['downloads']));
				$tmp = '';
				foreach($array as $ar){
					$title = get_the_title($ar);
					$tmp .= ',<strong>'.$title.'</strong>';
				}
				$data[$cols]['downloads'] = trim($tmp,',');
			}
		}

		header_remove();
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

	    $output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	    die();

	}
	
}

$ab_cal_functions = new Functions_admin;

?>