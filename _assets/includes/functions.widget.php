<?php

class ab_res_widget extends WP_Widget {

	public function __construct() {
		parent::__construct('ab_res_widget', __('Resource Sidebar Widget', 'text_domain'), 
			array( 'description' => __( 'Select which resource item to appear on the blog sidebar', 'text_domain' ), ) 
		);
	}

	public function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance['title'] );

		$res = get_post( $instance['resource'], OBJECT );

		$url = get_post_meta( $instance['resource'], 'AB_resources_downloadLink', true );

		include('templates/front/sidebar-resources.php');
	}
		
	public function form( $instance ) {
		global $wpdb;

		$selectedRes = 0;

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}else {
			$title = __( 'New title', 'text_domain' );
		}

		if ( isset( $instance[ 'resource' ] ) ) {
			$selectedRes = $instance[ 'resource' ];
		}	

		$resources = $wpdb->get_results("SELECT * FROM ".$wpdb->posts." WHERE post_type = 'resources' AND post_status='publish'");

		include('templates/admin/resources_admin_widget.php');
	}
			
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['resource'] = ( ! empty( $new_instance['resource'] ) ) ? strip_tags( $new_instance['resource'] ) : '';

		return $instance;
	}
}
?>