<?php

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Functions_customTable extends WP_List_Table {

    var $eDate = 0;

    function __construct($eDate){
        global $status, $page;

        $this->eDate = $eDate;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'contact',
            'plural'    => 'contacts',
            'ajax'      => false
        ) );
        
    }

    function display_tablenav( $which ) 
    {
        ?>
        <div class="tablenav <?php echo esc_attr( $which ); ?>">

            <div class="alignleft actions">
                <?php $this->bulk_actions(); ?>
            </div>
            <?php
            $this->extra_tablenav( $which );
            $this->pagination( $which );
            ?>
            <br class="clear" />
        </div>
        <?php
    }

    function column_default($item, $column_name){
        switch($column_name){
            default:
                return $item[$column_name];
        }
    }

    function column_title($item){
        
        /*$actions = array(
            'edit'      => sprintf('<a href="?page=%s&action=%s&movie=%s">Edit</a>',$_REQUEST['page'],'edit',$item['ID']),
            'delete'    => sprintf('<a href="?page=%s&action=%s&movie=%s">Delete</a>',$_REQUEST['page'],'delete',$item['ID']),
        );*/

        $actions = array();
        
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
            $item['id'],
            $this->row_actions($actions)
        );
    }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'],
            $item['id']
        );
    }

    function get_sortable_columns($columns=null) {
        $sortable_columns = array();
        foreach($columns as $key => $col){
            $sortable_columns[$key] = array($key, false);
        }
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            //'delete'    => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }

    function extra_tablenav( $which ) {
        global $wpdb, $testiURL, $tablename, $tablet;
        $date = array();
        $year = date('Y');
        $month = date('n');
        $output = '';
        $oYear = date('Y', strtotime($this->eDate));
        $oMonth = date('n', strtotime($this->eDate));
        for($i=0;$i<=2;$i++){
            $lyear = $year - $i;
            if($lyear < $oYear){
                break;
            }
            $output .= '<optgroup label="'.$lyear.'">';
            if($lyear == date('Y')){
                $limit = $month;
            }else{
                $limit = $month = 12;
            }
            for($c=0;$c<=$limit;$c++){
                $lmonth = $month - $c;
                if($lmonth < $oMonth && $lyear == $oYear){
                    break;
                }
                $monthName = date("F", mktime(0, 0, 0, $lmonth, 10));
                $selected = false;
                if($lmonth.'-'.$lyear == $_GET['date']){
                    $selected = 'selected="selected"';
                }
                $output .= '<option value="'.$lmonth.'-'.$lyear.'" '.$selected.'>'.$monthName.'</option>';
            }
            $output .= '</optgroup>';
        }
        include_once('templates/admin/crm_filter.php');
    }

    function prepare_items($tableData=null,$columnData=null,$colSortable=null) {
        global $wpdb; //This is used only if making any database queries

        $per_page = 10;

        $columns = $columnData;
        $hidden = array();

        if(!$colSortable){
            $sortable = $this->get_sortable_columns($columnData);
        }else{
            $sortable = $colSortable;
        }

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $data = $tableData;

        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='desc') ? $result : -$result; //Send final sort direction to usort
        }

        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        $this->items = $data;
        
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
}
?>