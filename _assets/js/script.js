;(function( $ ) {

	$(document).ready(function(){

		$(".stretch").anystretch();

		$("#coo.stretch").anystretch({
			positionY: 50
		});

		$('#secondHeader').stickyheaders();

		$('#elvis').click(function() {
		    $(this).toggleClass('active');
		    $('#candid-photography-sketch').toggleClass('active');
		    $('.wrapper').toggleClass('slide-out');
		});

		if(BrowserDetect.browser == "Explorer" && BrowserDetect.version >= "9") {

			var highestCol = Math.max($('#hotjobs aside').height(),$('#hotjobs section').height());
			$('#hotjobs > section, #hotjobs > aside').height(highestCol);

			var highestCol = Math.max($('#hotjobs .fjob .stretch').height(),$('#hotjobs .fjob .data').height());
			$('#hotjobs .fjob .stretch').height(highestCol);

			var highestCol = Math.max($('#single-main aside').height(),$('#single-main section').height());
			$('#single-main > aside').height(highestCol);

			var highestCol = Math.max($('#archive .pulltop').height(),$('#archive .data').height());
			highestCol = highestCol-20;
			$('#archive .pulltop').height(highestCol);

		}


		var Util = {
			is_webkit: function() {
				return navigator.userAgent.indexOf("AppleWebKit") > -1;
			},
			message: function() {
				if ( typeof console == "object" ) {
				}
			}
		}
		 
		// call on page load
		Util.message();


		if( !($('html').hasClass('boxsizing')) ){
			$('.boxSized, .boxSized *').each(function(){
				var fullW = $(this).outerWidth(),
					actualW = $(this).width(),
					wDiff = fullW - actualW,
					newW = actualW - wDiff;

				$(this).css('width',newW);
			});
		}

		$('#emailCheck').on('blur', function(){
			var data = {};
			data.email = $(this).val();
			SIB_checkemail(data, $(this));
		});

		$('#resource-SignUp').on('submit', function(){
			SIB_signup($(this));
			return false;
		});

		$('#mc-embedded-subscribe-form').on('submit', function(){
			SIB_subscribe($(this));
			return false;
		});

	});

	function SIB_checkemail(data, target){
		var l = Ladda.create( document.querySelector( '#loading' ) );
		l.start();
		$.ajax({
			url:ajaxurl, 
			type: 'POST',
			data: {
				action: 'ab_cal_SIB_emailCheck',
				data: data
			}, 
			dataType: 'JSON',
			complete:function(response) {
				l.stop();
				var status = JSON.parse(response.responseJSON.message.body);
				if(status.code === 'success'){
					target.attr('disabled', 'disabled').addClass('success');
					target.parents('form').find('.success-hide').fadeOut();
					target.parents('form').find('span.hide').fadeIn();
				}
			}
		});
	}

	function SIB_signup(target){
		var l = Ladda.create( document.querySelector( '#loading' ) );
		l.start();
		var redirect = target.find('#rurl').val();
		var data = target.serialize();
		$.ajax({
			url:ajaxurl, 
			type: 'POST',
			data: {
				action: 'ab_cal_SIB_signup',
				data: data
			}, 
			dataType: 'JSON',
			complete:function(response) {
				l.stop();
				var status = JSON.parse(response.responseJSON.message.body);
				//var status = response.responseJSON.message.body.status.code;
				//console.log(status);
				if(status.code === 'success'){
					var nonce = $('#_verify').val();
					var resid = $('#resid').val();
					window.location.replace('/thankyou?ref='+nonce+'&resid='+resid);
				}
			}
		});
	}

	function SIB_subscribe(target){
		var l = Ladda.create( document.querySelector( '#loading' ) );
		l.start();
		var redirect = target.find('#rurl').val();
		var data = target.serialize();
		$.ajax({
			url:ajaxurl, 
			type: 'POST',
			data: {
				action: 'ab_cal_SIB_subscribe',
				data: data
			}, 
			dataType: 'JSON',
			complete:function(response) {
				l.stop();
				var status = JSON.parse(response.responseJSON.message.body);
				//var status = response.responseJSON.message.body.status.code;
				//console.log(status);
				if(status.code === 'success'){
					$('#confirm').fadeIn();
				}
			}
		});
	}
	
})( jQuery );
