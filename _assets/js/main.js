window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; args.callee = args.callee.caller; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());

if(!this.jQuery) {
	document.write(unescape('%3Cscript src="assets/js/libs/jquery-1.11.0.min.js"%3E%3C/script%3E'));
}

document.write(unescape('%3Cscript src="/js/libs/jquery.placeholder.min.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/selectivizr-min.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/jquery.anystretch.min.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/jquery.lazyload.min.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/jquery.fitvids.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/rem.min.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/flexie.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/jquery.stickyheaders.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/spin.min.js"%3E%3C/script%3E'));
document.write(unescape('%3Cscript src="/js/libs/ladda.js"%3E%3C/script%3E'));

/*;(function($){if(typeof $.browser==="undefined"||!$.browser){var browser={};$.extend(browser);}var pluginList={flash:{activex:"ShockwaveFlash.ShockwaveFlash",plugin:/flash/gim},sl:{activex:["AgControl.AgControl"],plugin:/silverlight/gim},pdf:{activex:"PDF.PdfCtrl",plugin:/adobe\s?acrobat/gim},qtime:{activex:"QuickTime.QuickTime",plugin:/quicktime/gim},wmp:{activex:"WMPlayer.OCX",plugin:/(windows\smedia)|(Microsoft)/gim},shk:{activex:"SWCtl.SWCtl",plugin:/shockwave/gim},rp:{activex:"RealPlayer",plugin:/realplayer/gim},java:{activex:navigator.javaEnabled(),plugin:/java/gim}};var isSupported=function(p){if(window.ActiveXObject){try{new ActiveXObject(pluginList[p].activex);$.browser[p]=true;}catch(e){$.browser[p]=false;}}else{$.each(navigator.plugins,function(){if(this.name.match(pluginList[p].plugin)){$.browser[p]=true;return false;}else{$.browser[p]=false;}});}};$.each(pluginList,function(i,n){isSupported(i);});})(jQuery);*/


;(function($) {
  var cache = [];
  // Arguments are image paths relative to the current page.
  $.preLoadImages = function() {
    var args_len = arguments.length;
    for (var i = args_len; i--;) {
      var cacheImage = document.createElement('img');
      cacheImage.src = arguments[i];
      cache.push(cacheImage);
    }
  }
})(this.jQuery)

//jQuery.preLoadImages("image1.gif", "/path/to/image2.png");


if(this.BrowserDetect.browser == "Explorer" && this.BrowserDetect.version == "6") {
	for(i = 0; i < document.styleSheets.length; i++) {
		document.styleSheets.item(i).disabled = true;
	}
	document.write(unescape('You are using an out of date browser, please update your browser at http://www.browsehappy.com'));
}else{
	var framework = {
		
		init:function() {
			if(navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
				var viewportmeta = document.querySelectorAll('meta[name="viewport"]')[0];
				
				if(viewportmeta) {
					viewportmeta.content = "width=device-width,minimum-scale=1.0,maximum-scale=1.0";
					
					document.body.addEventListener("gesturestart",function() {
						viewportmeta.content = "width=device-width,minimum-scale=0.25,maximum-scale=1.6";
					},false);
				}
			}else{
				$("img").lazyload({
					effect:"fadeIn"
				});

				$("#single-main #content #article").fitVids();
				
				if($("input").length) {
					$("input").placeholder();
				}
				
				$("a[href$='.zip'],a[href$='.txt'],a[href$='.docx'],a[href$='.doc'],a[href$='.xls'],a[href$='.xlsx'],a[href$='.pdf'],a[href$='.ppt'],a[href$='.pptx']").bind("click",function() {
					window.open(this.href);
					
					return false;
				});

				Modernizr.addTest("boxsizing", function() {
					return Modernizr.testAllProps("boxSizing") && (document.documentMode === undefined || document.documentMode > 7);
				});



			}
		}
	}
	// Some times add-ons, extension and plugin developers bundle a version of jQuery
	var $ = this.jQuery.noConflict();
	
	$(document).ready(function() {
		framework.init();
	});
}


;(function( $ ) {

	$(document).ready(function(){

		$(".stretch").anystretch();

		$("#coo.stretch").anystretch({
			positionY: 50
		});

		$('#secondHeader').stickyheaders();

		$('#elvis').click(function() {
		    $(this).toggleClass('active');
		    $('#candid-photography-sketch').toggleClass('active');
		    if($(this).hasClass('active')){
		    	$('#candid-photography-sketch').css('z-index','9999');
			}else{
				$('#candid-photography-sketch').css('z-index','-999');
			}
		    $('.wrapper').toggleClass('slide-out');
		});

		if(BrowserDetect.browser == "Explorer" && BrowserDetect.version >= "9") {

			var highestCol = Math.max($('#hotjobs aside').height(),$('#hotjobs section').height());
			$('#hotjobs > section, #hotjobs > aside').height(highestCol);

			var highestCol = Math.max($('#hotjobs .fjob .stretch').height(),$('#hotjobs .fjob .data').height());
			$('#hotjobs .fjob .stretch').height(highestCol);

			var highestCol = Math.max($('#archive .pulltop').height(),$('#archive .data').height());
			highestCol = highestCol-20;
			$('#archive .pulltop').height(highestCol);

		}


		var Util = {
			is_webkit: function() {
				return navigator.userAgent.indexOf("AppleWebKit") > -1;
			},
			message: function() {
				if ( typeof console == "object" ) {
				}
			}
		}
		 
		// call on page load
		Util.message();


		if( !($('html').hasClass('boxsizing')) ){
			$('.boxSized, .boxSized *').each(function(){
				var fullW = $(this).outerWidth(),
					actualW = $(this).width(),
					wDiff = fullW - actualW,
					newW = actualW - wDiff;

				$(this).css('width',newW);
			});
		}

		$('#emailCheck').on('blur', function(){
			var data = {};
			data.email = $(this).val();
			SIB_checkemail(data, $(this));
		});

		$('#resource-SignUp').on('submit', function(){
			SIB_signup($(this));
			return false;
		});

		$('#mc-embedded-subscribe-form').on('submit', function(){
			SIB_subscribe($(this));
			return false;
		});

	});

	function SIB_checkemail(data, target){
		var l = Ladda.create( document.querySelector( '#loading' ) );
		l.start();
		$.ajax({
			url:ajaxurl, 
			type: 'POST',
			data: {
				action: 'ab_cal_SIB_emailCheck',
				data: data
			}, 
			dataType: 'JSON',
			complete:function(response) {
				l.stop();
				var status = JSON.parse(response.responseJSON.message.body);
				if(status.code === 'success'){
					target.addClass('success');
					target.parents('form').find('.success-hide').fadeOut();
					target.parents('form').find('span.hide').fadeIn();
				}
			}
		});
	}

	function SIB_signup(target){
		var l = Ladda.create( document.querySelector( '#loading' ) );
		l.start();
		var redirect = target.find('#rurl').val();
		var data = target.serialize();
		$.ajax({
			url:ajaxurl, 
			type: 'POST',
			data: {
				action: 'ab_cal_SIB_signup',
				data: data
			}, 
			dataType: 'JSON',
			complete:function(response) {
				l.stop();
				console.log(response);
				var status = JSON.parse(response.responseJSON.message.body);
				//var status = response.responseJSON.message.body.status.code;
				//console.log(status);
				if(status.code === 'success'){
					var nonce = $('#_verify').val();
					var resid = $('#resid').val();
					window.location.replace('/thankyou?ref='+nonce+'&resid='+resid);
				}
			}
		});
	}

	function SIB_subscribe(target){
		var l = Ladda.create( document.querySelector( '#loading' ) );
		l.start();
		var redirect = target.find('#rurl').val();
		var data = target.serialize();
		$.ajax({
			url:ajaxurl, 
			type: 'POST',
			data: {
				action: 'ab_cal_SIB_subscribe',
				data: data
			}, 
			dataType: 'JSON',
			complete:function(response) {
				l.stop();
				var status = JSON.parse(response.responseJSON.message.body);
				//var status = response.responseJSON.message.body.status.code;
				//console.log(status);
				if(status.code === 'success'){
					$('#confirm').fadeIn();
				}
			}
		});
	}
	
})( jQuery );
