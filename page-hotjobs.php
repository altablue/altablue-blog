<?php get_header(); ?>
	
		<section id="single-main" role="main" class="container gapless hotjobs">

			<?php while(have_posts()): the_post(); ?>

			<section class="twelve left gapless data ">

				<section class="whole left gapless">	

					<article id="content" class="fourteen left gapless whitebg">
						<div class="whole gapless clearing"></div>
						<section id="article" class="clearing">
							<h2 class="whole centered gapless"><?php the_title(); ?></h2>
							<figure class="img whole gapless stretch" <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
								<noscript><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" /></noscript>
							</figure>
							<div class="whole gapless left"><?php the_content(); ?></div>
						</section>

						<div id="rikmayall" class="whole left gapless  homepage">
							<section class="whole gapless clearing">
								<?php $hotJobsSticky = $ab_cal_functions->ab_cal_get_sticky_hotjobs(); ?>
								<?php if($hotJobsSticky->have_posts()): while($hotJobsSticky->have_posts()): $hotJobsSticky->the_post(); ?>
									<div class="fjob whole clearing row gapless frame">
										<div class="door whole clearing gapless">
											<div class="left half gapless">
												<section class="door stretch no-flick clearing"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
													<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
												</section>
											</div>
											<div class="half pushright gapless data">
												<h3>Featured Job</h3>
												<h2><?php the_title(); ?></h2>
												<p class="clearing row"><?php the_excerpt(); ?></p>
												<a href="https://jobs.alta-blue.com/Pages/Vacancy.aspx?ID=IRC<?php echo get_post_meta( $post->ID, 'CPT_hotjobs_irec', 'true' ); ?>&Company=AB" target="_blank" class="row nine cta blue">View Job</a>
											</div>
										</div>
									</div>
									<hr class="black" />
								<?php endwhile; endif; ?>
							</section>
							<nav id="pagination" class="row centered fourteen gapless clearing">
								<section class="centered gapless">
									<a href="/hotjobs/viewall" class="half centered gapless" style="overflow:hidden;"><p>View More Hot Jobs&rarr;</p></a>
								</section>
							</nav>
						</div>
					</article>

				</section>

			</section>


			<aside id="sidebar" class="pushright gapless four">

				<?php get_template_part('sidebar'); ?>

			</aside>

			<?php endwhile; ?>

		</section>

<?php get_footer(); ?>