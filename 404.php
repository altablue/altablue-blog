<?php
/*
Template Name: Search Page
*/
?>

<?php get_header(); ?>

	<section id="main" role="main" class="container gapless fourohfour">

		<div id="coo" class="stretch whole gapless clearing" data-stretch="/wp-content/themes/altablue/_assets/imgs/404/coo.jpg">
			<section class="fourteen centered gapless">
				<div class="six gapless left">
					<h5>404</h5>
					<h3>Sorry&nbsp;Buddy,<br/><br/>That&nbsp;page<br/>cannot&nbsp;be&nbsp;found</h3>
				</div>
				<div class="seven gapless pushright">
					<p>Just like poor <span>Hamish the Altablue Coo</span>, it looks like you might be lost!</p>
					<p>Never fear, We’ll try get you and wee hamish back on your paths right away. Just scroll down and use the options below to return to our homepage, or try and find the page you were looking for before.</p>
				</div>
			</section>
			<noscript><figure><img class="whole gapless" src="/wp-content/themes/altablue/_assets/imgs/404/coo.jpg" /></figure></noscript>
		</div>

		<hr class="black" />

		<div id="action" class="stretch" data-stretch="/wp-content/themes/altablue/_assets/imgs/404/map.jpg">
			<section class="fourteen centered gapless">
				<h2>Let us get you back on your journey:</h2>
				<p>Use the search below to find the page you were orginally looking for:</p>
				<form class="seven gapless centered clearing" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search" method="POST">
					<input type="text" class="thirteen" name="s" placeholder="enter your search query" /><span></span><button class="cta blue small-cta" type="submit">search</button>
				</form>
				<section class="clearing">
					<hr class="seven left gapless"/>
					<div class="two gapless icon-or"></div>
					<hr class="seven gapless pushright" />
				</section>
				<p>Use the buttons below to return to our homepage to view the site’s sitemap:</p>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="cta orange">homepage</a><div class="one gapless spacer"></div><a href="<?php echo esc_url( home_url( '/sitemap' ) ); ?>" class="cta orange">view sitemap</a>
			</section>
			<div class="black-overlay"></div>
			<noscript><figure><img class="whole gapless" src="/wp-content/themes/altablue/_assets/imgs/404/map.jpg" /></figure></noscript>
		</div>

	</section>

<?php get_footer(); ?>