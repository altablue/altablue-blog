<?php get_header(); ?>
	
		<section id="single-main" role="main" class="container gapless">

			<?php while(have_posts()): the_post(); ?>

			<section class="twelve left gapless data bgwhite">

				<section class="whole left gapless">	

					<article id="content" class="fourteen left gapless whitebg">
						<div class="whole gapless clearing"></div>
						<section id="article" class="clearing">
							<h2 class="whole centered gapless"><?php the_title(); ?></h2>
							<figure class="img whole gapless stretch" <?php if ( has_post_thumbnail() ) : ?> data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
								<noscript><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" /></noscript>
							</figure>
							<div class="whole gapless left"><?php the_content(); ?></div>
						</section>

						<?php $ab_cal_functions->ab_cal_the_post_cta(); ?>
					</article>

				</section>

			</section>


			<aside id="sidebar" class="pushright gapless four">

				<?php get_template_part('sidebar'); ?>

			</aside>

			<?php endwhile; ?>

		</section>

<?php get_footer(); ?>