<?php get_header(); ?>
	
		<section id="single-main" role="main" class="subscribe container gapless">

			<?php while(have_posts()): the_post(); ?>

			<section class="twelve left gapless data bgwhite">

				<section class="whole left gapless">	

					<article id="content" class="fourteen left gapless whitebg">
						<div class="whole gapless clearing"></div>
						<section id="article" class="clearing">
							<h2 class="whole centered gapless"><?php the_title(); ?></h2>
							<figure class="img whole gapless stretch" <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
								<noscript><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" /></noscript>
							</figure>
							<div class="whole gapless left">

								<?php the_content(); ?>

								<form class="centered twelve gapless" action="https://my.sendinblue.com/users/subscribe/js_id/1u0yu/id/1" method="POST" id="mc-embedded-subscribe-form" name="theform" class="validate" onsubmit="return false;" novalidate>
									<input type="hidden" name="listid" value="16" />
									<input type="hidden" name="1u0yu" value="js_id" />
									<div class="hidden hide whole clearing" id="confirm">
										<span class="clearing">You've been subscribed to the Altablue News Blog, Hurrah!</span>
									</div>
									<div class="clearing success-hide">
									    <input class="whole left gapless" placeholder="Your first name" name="NAME" type="text">
									    <!--<span class="one pushright gapless icon icon-fname"></span>-->
									</div>

									<div class="clearing success-hide ">
									    <input class="whole left gapless" placeholder="Your last name" name="SURNAME" type="text">
									    <!--<span class="one pushright gapless icon icon-fname"></span>-->
									</div>

									<div class="row whole clearing gapless">
										<span class="hide clearing">Hey, looks like you've signed up already, awesome!</span>
										<input placeholder="e.g john.smith@myemail.com" class="whole left gapless" type="email" id="emailCheck" name="email" required id="email" />
										<!--<span class="one pushright gapless icon icon-email-address"></span>-->
									</div>
									<div class="row whole clearing gapless">
										<button id="loading" class="whole centered gapless cta orange ladda-button" type="submit" data-style="expand-right"><span class="ladda-label">Subscribe</button>
									</div>

								    <div style="position: absolute; left: -5000px;"><input type="text" name="b_043ba4fa1df9c6d918079baaf_c8773da3a2" tabindex="-1" value=""></div>
								</form>

							</div>
						</section>

					</article>

				</section>

			</section>


			<aside id="sidebar" class="pushright gapless four">

				<?php get_template_part('sidebar'); ?>

			</aside>

			<?php endwhile; ?>

		</section>

<?php get_footer(); ?>