<?php 
/*
	Template Name: Sitemap
*/
?>

<?php get_header(); ?>

		<section id="single-main" role="main" class="site-map container gapless">

			<aside id="single-sidebar" class="left four blackbg gapless">
				<hr class="black" />
				<?php 
					$taxonomies = array( 
					    'category',
					);
					$args = array(
					    'orderby'           => 'count', 
					    'order'             => 'DESC',
					); 
					$terms = get_terms($taxonomies, $args);
					foreach($terms as $term):
	 					$latest_cat_post = new WP_Query( array('posts_per_page' => 1, 'category__in' => array($term->term_id)));
						if( $latest_cat_post->have_posts() ) : while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();  ?>
							<section class="clearing frame">
								<h5><?php echo $term->name; ?></h5>
								<a href="<?php echo get_the_permalink($latest_cat_post->post->ID); ?>" class="door">
									<figure class="img whole gapless"><?php if ( has_post_thumbnail($latest_cat_post->post->ID) ) : ?><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($latest_cat_post->post->ID) ); ?>" /><?php endif; ?></figure>	
									<div class="post_data">
										<h4><?php the_title(); ?></h4>	
										<?php echo wp_trim_words(get_the_content(), 30, ''); ?>
									</div>
								</a>
							</section>
				<?php endwhile; endif; endforeach;?>
			</aside>


			<?php while(have_posts()): the_post(); ?>

			<section class="twelve pushright gapless data bgwhite">

				<section class="whole pushright gapless">	

					<article id="content" class="fourteen pushright gapless whitebg">
						<div class="whole gapless clearing"></div>
						<section id="article">
							<h2 class="fifteen centered gapless"><?php the_title(); ?></h2>
							<div class="fifteen gapless left"><?php the_content(); ?></div>
						</section>
					</article>

				</section>

			</section>

			<?php endwhile; ?>

		</section>

<?php get_footer(); ?>