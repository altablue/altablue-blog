<section class="clearing">
	<h3>Subscribe</h3>
	<hr />
	<a class="cta cta-orange orange" href="/subscribe">reasons to subscribe &rarr;</a>
</section>

<section>
	<h3>Search</h3>
	<hr />
	<?php echo get_search_form( $echo ); ?>
</section>

<section>
	<h3>categories</h3>
	<hr />
	<ul>
		<?php wp_list_categories(array('title_li' => '', 'show_count'=>true)); ?>
	</ul>
</section>

<section id="res" class="clearing">
	<?php /*<h3>resources</h3>
	<hr />
	<figure class="third left gapless">
		<img src="/wp-content/themes/altablue/_assets/imgs/holding/book-holding.jpg" />
	</figure>
	<div class="twothirds pushright gapless content info">
		<h5>10 ways to improve everything, ever!</h5>
		<a href="#" class="cta-orange cta orange">download</a>
	</div>*/?>
	<?php dynamic_sidebar('sidebar'); ?>
</section>

<section class="clearing">
	<h3>hotjobs</h3>
	<hr />
	<p>Check out the latest selection of Altablue Hot Jobs with locations based around the world.</p>
	<a class="cta cta-orange orange" href="/hotjobs">checkout Hot Jobs &rarr;</a>
</section>

<section id="socialside">
	<h3>social</h3>
	<hr/>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=205775849509142";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div class="fb-page" data-href="https://www.facebook.com/pages/Altablue/811696008908833" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pages/Altablue/811696008908833"><a href="https://www.facebook.com/pages/Altablue/811696008908833">Altablue</a></blockquote></div></div>
	<hr/>
	<a class="twitter-timeline" href="https://twitter.com/alta_blue" width="100%" data-widget-id="589004056993083392">Tweets by @alta_blue</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	<hr/>
	<div class="whole gapless centered">

		<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
		<script type="IN/FollowCompany" data-id="3750570" data-counter="top"></script>

		<div class="g-follow" data-annotation="vertical-bubble" data-height="20" data-href="https://plus.google.com/116991851462340448399" data-rel="publisher"></div>
		<script type="text/javascript">
		  window.___gcfg = {lang: 'en-GB'};

		  (function() {
		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    po.src = 'https://apis.google.com/js/platform.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
		<script src="https://apis.google.com/js/platform.js" async defer>
		  {lang: 'en-GB'}
		</script>

		<div class="fb-like" data-href="http://www.alta-blue.com" data-layout="box_count" data-action="recommend" data-show-faces="false" data-share="false"></div>
	</div>
</section>