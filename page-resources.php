<?php get_header(); ?>
	
		<section id="single-main" role="main" class="container resources gapless hotjobs">

			<?php while(have_posts()): the_post(); ?>

			<section class="twelve left gapless data ">

				<section class="whole left gapless">	

					<article id="content" class="fourteen left gapless whitebg">
						<div class="whole gapless clearing"></div>
						<section id="article" class="clearing">
							<h2 class="whole centered gapless"><?php the_title(); ?></h2>
							<figure class="img whole gapless stretch" <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
								<noscript><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" /></noscript>
							</figure>
							<div class="whole gapless left"><?php the_content(); ?></div>
						</section>

						<div id="rikmayall" class="whole left gapless  homepage">
							<section class="whole gapless clearing">
								<?php $resources = $ab_cal_functions->ab_cal_get_resources_list(); ?>
								<?php if($resources->have_posts()): while($resources->have_posts()): $resources->the_post(); ?>
									<div class="fjob whole clearing row gapless frame">
										<div class="door whole clearing gapless">
											<div class="left half gapless">
												<section class="door stretch no-flick clearing"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
													<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
												</section>
											</div>
											<div class="pushright half gapless data">
												<h2><?php the_title(); ?></h2>
												<p class="clearing row"><?php the_excerpt(); ?></p>
												<a href="<?php echo the_permalink(); ?>" class="row cta blue">Download</a>
											</div>
										</div>
									</div>
									<hr class="black" />
								<?php endwhile; endif; ?>
							</section>
						</div>
					</article>

				</section>

			</section>


			<aside id="sidebar" class="pushright gapless four">

				<?php get_template_part('sidebar'); ?>

			</aside>

			<?php endwhile; ?>

		</section>

<?php get_footer(); ?>