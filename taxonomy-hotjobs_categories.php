<?php get_header(); ?>

	<?php //print_r($ab_cal_functions->ab_cal_makeBitlyUrl("http://www.alta-blue.com"));?>

	<section id="main" class="container gapless homepage">

		<section id="latest-posts" class="left gapless twelve">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<div class="row whole clearing gapless">
					
					<article id="post-<?php echo get_the_ID(); ?>" class="latest centered fourteen left gapless">
						<a href="https://jobs.alta-blue.com/Pages/Vacancy.aspx?ID=IRC<?php echo get_post_meta( $post->ID, 'CPT_hotjobs_irec', 'true' ); ?>&Company=AB" target="_blank">
							<section class="door stretch no-flick clearing"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
								<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
							</section>
							<div class="preview">
								<h3><?php the_title(); ?></h3>
								<div class="data whole gapless">
									<?php the_content(); ?>
								</div>
								<span class="cta cta-orange orange">view job</span>
							</div>
						</a>
					</article>

				</div>

			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

			<nav id="pagination" class="row centered fourteen gapless clearing">
				<?php if(get_previous_posts_link()): ?>
					<section class="prev eight left reverse frame gapless">
						<a href="<?php echo (get_previous_posts_link()? esc_url( get_previous_posts_page_link()) :'');?>" class="whole gapless" style="overflow:hidden;"><p>&larr; Older Posts</p></a>
					</section>
				<?php endif; ?>
				<?php if(get_next_posts_link()): ?>
					<section class="next eight frame pushright gapless">
						<a href="<?php echo (get_next_posts_link()? esc_url( get_next_posts_page_link()) :'');?>" class="gapless whole" style="overflow:hidden;"><p>Newer Posts &rarr;</p></a>
					</section>
				<?php endif; ?>
			</nav>

		</section>

		<aside id="sidebar" class="pushright gapless four">

			<?php get_template_part('sidebar'); ?>

		</aside>

	</section>

<?php get_footer(); ?>