<?php get_header(); ?>
	
		<section id="single-main" role="main" class="resources container gapless">

			<?php while(have_posts()): the_post(); ?>

				<section class="thirteen centered row gapless clearing hero">
					<h2 class="whole centered gapless"><?php the_title(); ?></h2>
					<p><?php the_excerpt(); ?></p>
				</section>

				<section class="ten left gapless data bgwhite">

					<section class="whole left gapless">	

						<article id="content" class="fourteen left gapless whitebg">
							<div class="whole gapless clearing"></div>
							<section id="article" class="clearing">
								<figure class="img whole gapless stretch" <?php if ( has_post_thumbnail() ) : ?> data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
									<noscript><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" /></noscript>
								</figure>
								<div class="whole gapless left"><?php the_content(); ?></div>
							</section>

							<?php $ab_cal_functions->ab_cal_the_post_cta(); ?>
						</article>

					</section>

				</section>


				<aside id="sidebar" class="pushright gapless six">
					<form id="resource-SignUp" target="_blank" action="https://my.sendinblue.com/users/subscribe/js_id/1u0yu/id/1" onsubmit="return false;" method="post">

		                <input type="hidden" name="js_id" id="js_id" value="1u0yu"/>
		                <input type="hidden" name="listid" id="listid" value="16"/>
		                <input type="hidden" name="from_url" id="from_url" value="yes" />
		                <input type="hidden" name="hdn_email_txt" id="hdn_email_txt" value=""/>

						<h3 class="whole centered">Yes, I want this download please!</h3>
						<div class="row whole clearing gapless">
							<label class="whole success-hide clearing gapless" for="email">Email</label>
							<span class="hide clearing">Hey, looks like you've signed up already, awesome!</span>
							<input placeholder="e.g john.smith@myemail.com" class="whole clearing gapless" type="email" id="emailCheck" name="email" required id="email" />
						</div>
						<div class="row success-hide whole clearing gapless">
							<label class="whole clearing gapless" for="fname">First name</label>
							<input placeholder="e.g John" class="whole clearing gapless" id="fname" type="text" name="NAME" />
						</div>
						<div class="row success-hide whole clearing gapless">
							<label class="whole clearing gapless" for="fname">Last name</label>
							<input placeholder="e.g Smith" class="whole clearing gapless" id="lname" type="text" name="SURNAME" />
						</div>
						<div class="row success-hide whole clearing gapless">
							<label class="whole clearing gapless" for="lname">Location</label>
							<input placeholder="e.g Aberdeen, Scotland" class="whole clearing gapless" type="text" name="LOCATION" id="lname" />
						</div>
						<div class="row success-hide whole clearing gapless">
							<label class="whole clearing gapless" for="cname">Company Name</label>
							<input placeholder="e.g My Company Ltd" class="whole clearing gapless" type="text" name="COMPANY" id="cname" />
						</div>
						<div class="row success-hide whole clearing gapless">
							<label class="whole clearing gapless" for="role">Role</label>
							<input placeholder="e.g Marketing Director" class="whole clearing gapless" type="text" name="ROLE" id="role" />
						</div>
						<div class="row success-hide whole clearing">
							<input contenteditable='false' type="checkbox" id="subscribe" name="subscribe" value="true" />
							<label for="subscribe">Subscribe Me to Altablue Blog Please</label>
						</div>
						<?php wp_nonce_field( '_resources_verify', '_verify' ); ?>
						<input type="hidden" id="resid" name="resid" value="<?php the_ID();?>" />
						<div class="row whole clearing gapless">
							<button id="loading" class="whole centered gapless cta orange ladda-button" type="submit" data-style="expand-right"><span class="ladda-label">Download Now</button>
						</div>
						<p class="whole clearing row centered">At Altablue, we are very committed to protecting your <a href="http://www.alta-blue.com/privacy" target="_blank">privacy</a>.</p>
					</form>
				</aside>

			<?php endwhile; ?>

		</section>

<?php get_footer(); ?>