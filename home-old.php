<?php get_header(); ?>

	<?php //print_r($ab_cal_functions->ab_cal_makeBitlyUrl("http://www.alta-blue.com"));?>

	<section id="main" class="container gapless homepage">

		<section id="latest-posts" class="left gapless whole blackbg">

			<?php $i=1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<?php if($i==1): ?>

					<div class="row whole clearing gapless">
						
						<article id="post-<?php echo get_the_ID(); ?>" class="latest ten left gapless">
							<a href="<?php echo get_the_permalink(); ?>">
								<section class="door stretch no-flick"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
									<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
								</section>
								<div class="preview">
									<div class="data whole gapless">
										<?php echo wp_trim_words(get_the_content(), 30, ' ... ');?>
									</div>
									<span class="cta cta-orange orange">READ MORE</span>
								</div>
							</a>
						</article>

						<section id="hotjobs" class="static six pushright gapless small">
								<article id="page-hotjobs" class="whole left gapless small">
									<a href="/hotjobs" class="small">
										<section class="door stretch no-flick"  data-stretch="/wp-content/themes/altablue/_assets/imgs/homepage/mug.jpg" >
											<noscript><img class="whole gapless" src="/wp-content/themes/altablue/_assets/imgs/homepage/mug.jpg" /></noscript>
										</section>
										<div class="preview">
											<div class="data whole gapless">
												<h3>Latest Hot Jobs</h3>
												<p>Check out the latest selection of Altablue Hot Jobs with locations based around the world.</p>
											</div>
											<span class="cta cta-orange orange">VIEW HOT JOBS</span>
										</div>
									</a>
								</article>
						</section>

						<section id="staticlink" class="static six pushright gapless small">
								<article id="post-<?php echo get_the_ID(); ?>" class="whole left gapless small">
									<a href="https://jobs.alta-blue.com" target="_blank" class="small">
										<section class="door stretch no-flick"  data-stretch="/wp-content/themes/altablue/_assets/imgs/homepage/jobsboard.jpg" >
											<noscript><img class="whole gapless" src="/wp-content/themes/altablue/_assets/imgs/homepage/jobsboard.jpg" /></noscript>
										</section>
										<div class="preview">
											<div class="data whole gapless">
												<h3>Job Board</h3>
												<p>View and apply for all available Altablue jobs and positions on our Altablue Jobs Board.</p>
											</div>
											<span class="cta cta-orange orange">VIEW JOBS</span>
										</div>
									</a>
								</article>
						</section>

					</div>

				<?php else: ?>
					<?php // if($i <= 3): ?>
						<article id="post-<?php echo get_the_ID(); ?>" class="third left gapless">
							<a href="<?php echo get_the_permalink(); ?>">
								<section class="door stretch no-flick"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
									<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
								</section>
								<div class="preview">
									<div class="data whole gapless">
										<?php echo the_title(); //wp_trim_words(get_the_content(), 30, ' ... ');?>
									</div>
									<span class="cta cta-orange orange">READ MORE</span>
								</div>
							</a>
						</article>
					<?php /* elseif($i == 4): ?>
						<article id="post-<?php echo get_the_ID(); ?>" class="twothirds left gapless">
							<a href="<?php echo get_the_permalink(); ?>">
								<section class="door stretch no-flick"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
									<div class="data fourteen gapless">
										<p class="meta"><?php echo get_the_date(); ?><span>/</span><?php $cat = get_the_category(get_the_ID()); echo $cat[0]->cat_name; ?><span>/</span><?php echo get_the_author(); ?></p>
										<h3><?php the_title(); ?></h3>
										<?php echo wp_trim_words(get_the_content(), 30, ' ... ');?>
									</div>
									<div class="gradient"></div>
									<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
								</section>
							</a>
						</article>
					<?php else: ?>
						<article id="post-<?php echo get_the_ID(); ?>" class="third left gapless">
							<a href="<?php echo get_the_permalink(); ?>">
								<section class="door stretch no-flick"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
									<div class="data fourteen gapless">
										<p class="meta"><?php echo get_the_date(); ?><span>/</span><?php $cat = get_the_category(get_the_ID()); echo $cat[0]->cat_name; ?><span>/</span><?php echo get_the_author(); ?></p>
										<h3><?php the_title(); ?></h3>
										<?php echo wp_trim_words(get_the_content(), 30, ' ... ');?>
									</div>
									<div class="gradient"></div>
									<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
								</section>
							</a>
						</article>
					<?php endif; */?>
				<?php endif; ?>

			<?php $i++; endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

		</section>

		<?php /* <aside id="sidebar" class="pushright gapless six blackbg">
		
			
			<?php 
				$i = 1;
				$featured_posts = $featuredContent->get_featured_posts();
				foreach ( (array) $featured_posts as $order => $post ) :  
					//setup_postdata( $post );
					if($i == 1):?>
					
						<article id="post-<?php echo get_the_ID(); ?>" class="whole left gapless">
							<a href="<?php echo get_the_permalink(); ?>">
								<section class="door stretch no-flick"  <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
									<div class="data fourteen gapless">
										<p class="meta"><?php echo get_the_date(); ?><span>/</span><?php $cat = get_the_category($post->ID); echo $cat[0]->cat_name; ?><span>/</span><?php echo get_the_author_meta('display_name', $post->post_author); ?></p>
										<h3><?php the_title(); ?></h3>
										<?php echo wp_trim_words($post->post_content, 30, ' ... ');?>
									</div>
									<div class="gradient"></div>
									<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
								</section>
							</a>
						</article>
			<?php elseif($i == 2): ?>
					
						<a href="<?php echo get_the_permalink(); ?>" class="whole left gapless small">
							<div class="door no-flick">
								<section class="half left gapless">
									<div class="gradient"></div>
									<div class="stretch" <?php if ( has_post_thumbnail() ) : ?>data-stretch="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" <?php endif; ?>>
										<noscript><?php if ( has_post_thumbnail() ) : ?><img class="whole gapless" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"/><?php endif; ?></noscript>
									</div>
								</section>
								<article id="post-<?php echo $post->ID; ?>" class="half left gapless">
									<section> 
										<div class="data fourteen gapless">
											<p class="meta"><?php echo get_the_date(); ?><span>/</span><?php $cat = get_the_category($post->ID); echo $cat[0]->cat_name; ?></p>
											<h3><?php the_title(); ?></h3>
											<p><?php echo wp_trim_words($post->post_content, 12, ' ... '); ?></p>
										</div>
									</section>
								</article>
							</div>
						</a>
			<?php else: ?>

						<a href="<?php echo get_the_permalink(); ?>" class="half left gapless small no-pic">
							<article id="post-<?php echo $post->ID; ?>" class="door no-flick">
								<section> 
									<div class="data fourteen gapless">
										<p class="meta"><?php echo get_the_date(); ?><span>/</span><?php $cat = get_the_category(get_the_ID()); echo $cat[0]->cat_name; ?></p>
										<h3><?php the_title(); ?></h3>
										<p><?php echo wp_trim_words($post->post_content, 12, ' ... '); ?></p>
									</div>
								</section>
							</article>
						</a>

			<?php endif; $i++; endforeach; ?>
			


		</aside> */ ?>

		<hr class="black" />

		<nav id="pagination" class="row whole gapless clearing">
			<section class="prev eight left reverse frame gapless">
				<a href="<?php echo (get_previous_posts_link()? esc_url( get_previous_posts_page_link()) :'');?>" class="whole door stretch gapless" data-stretch="/wp-content/themes/altablue/_assets/imgs/homepage/olderposts.jpg" style="overflow:hidden;"><p>Older Posts</p><noscript><img class="whole gapless" src="/wp-content/themes/altablue/_assets/imgs/homepage/olderposts.jpg" /></noscript></a>
			</section>
			<section class="next eight frame pushright gapless">
				<a href="<?php echo (get_next_posts_link()? esc_url( get_next_posts_page_link()) :'');?>" class="gapless whole door stretch" data-stretch="/wp-content/themes/altablue/_assets/imgs/homepage/newerposts.jpg" style="overflow:hidden;"><p>Newer Posts</p><noscript><img class="whole gapless" src="/wp-content/themes/altablue/_assets/imgs/homepage/newerposts.jpg" /></noscript></a>
			</section>
		</nav>

		<hr class="black" />

	</section>

<?php get_footer(); ?>