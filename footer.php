
    <footer id="footer" class="container gapless">

      <section id="mailinglist" class="whole row gapless frame">
          <div class="ten gapless content centered clearing">
            <p class="eleven left">Join our mailing list to get the latest articles in your mailbox.</p>
            <a href="/subscribe/" class="cta orange cta-orange pushright five">sign up now</a>
          </div>
      </section>

      <section id="foot-top">
        <ul class="whole gapless row clearing">
          <li>
            <ul>
              <li class="third col">
                <div class="third left gapless icon-address"></div>
                <div class="twothirds pushright gapless"><address><p>Sixth Floor - New Telecom House<br/>73 College Street, Aberdeen<br/>AB11 6FD</p></address></div>
              </li>
            </ul>
          </li>
          <li>
            <ul>
              <li class="third col">
                <div class="third left gapless icon-phone"></div>
                <p class="twothirds gapless pushright"><a href="mailto:&#105;&#110;&#102;&#111;&#064;&#097;&#108;&#116;&#097;&#045;&#098;&#108;&#117;&#101;&#046;&#099;&#111;&#109;">info@alta-blue.com</a><br/><a href="tel:+44 (0)1224 777900">+44 (0)1224 777900</a></p>
              </li>
            </ul>
          </li>
          <li>
            <ul>
              <li class="third col pushright">
                <div class="third left gapless icon-linkedin"></div>
                <p class="twothirds pushright gapless"><a href="https://www.linkedin.com/company/altablue" target="_blank">connect with us on linkedin</a></p>
              </li>
            </ul>
          </li>
        </ul>
      </section>

      <section id="foot-small" class="clearing">
        <p class="six col">&copy; Copyright Altablue <?php echo date('Y'); ?>.</p>
        <nav class="four col pushright"><a href="http://www.alta-blue.com/terms.html" target="_blank">Terms of Service</a>|<a href="http://www.alta-blue.com/privacy.html" target="_blank">Privacy Policy</a>|<a href="http://www.alta-blue.com/cookies.html" target="_blank">Cookie Policy</a></nav>
      </section>

    </footer>

  </section>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
  <script src="/wp-content/themes/altablue/_assets/js/main.js"></script>
  <script type="text/javascript" src="data:text/javascript;base64,ICAoZnVuY3Rpb24oaSxzLG8sZyxyLGEsbSl7aVsnR29vZ2xlQW5hbHl0aWNzT2JqZWN0J109cjtpW3JdPWlbcl18fGZ1bmN0aW9uKCl7DQogIChpW3JdLnE9aVtyXS5xfHxbXSkucHVzaChhcmd1bWVudHMpfSxpW3JdLmw9MSpuZXcgRGF0ZSgpO2E9cy5jcmVhdGVFbGVtZW50KG8pLA0KICBtPXMuZ2V0RWxlbWVudHNCeVRhZ05hbWUobylbMF07YS5hc3luYz0xO2Euc3JjPWc7bS5wYXJlbnROb2RlLmluc2VydEJlZm9yZShhLG0pDQogIH0pKHdpbmRvdyxkb2N1bWVudCwnc2NyaXB0JywnLy93d3cuZ29vZ2xlLWFuYWx5dGljcy5jb20vYW5hbHl0aWNzLmpzJywnZ2EnKTsNCg0KICBnYSgnY3JlYXRlJywgJ1VBLTUyNDExNzA1LTcnLCAnYXV0bycpOw0KICBnYSgnc2VuZCcsICdwYWdldmlldycpOw=="></script>
  <?php wp_footer(); ?>
</body>
</html>